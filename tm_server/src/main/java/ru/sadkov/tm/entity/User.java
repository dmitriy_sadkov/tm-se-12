package ru.sadkov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.enumeration.Role;
import ru.sadkov.tm.util.RandomUtil;

import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@XmlType(name = "user")
public final class User extends AbstractEntity implements Serializable {

    @Nullable
    private String login;
    @Nullable
    private String password;
    @Nullable
    private Role role;

    public User(@NotNull final String login, @NotNull final String password, @NotNull final Role role) {
        this.login = login;
        this.password = password;
        this.role = role;
        this.id = RandomUtil.UUID();
    }

    @Override
    @NotNull
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", login='" + login + '\'' +
                ", role=" + role +
                '}';
    }
}
