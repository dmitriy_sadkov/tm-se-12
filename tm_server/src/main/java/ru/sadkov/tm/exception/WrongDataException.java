package ru.sadkov.tm.exception;


import org.jetbrains.annotations.NotNull;

public class WrongDataException extends Exception {

    public WrongDataException() {
        super("[YOU ENTER WRONG DATA]");
    }

    public WrongDataException(@NotNull final String message) {
        super(message);
    }
}
