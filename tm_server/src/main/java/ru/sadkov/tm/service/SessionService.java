package ru.sadkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.api.ISessionRepository;
import ru.sadkov.tm.api.ISessionService;
import ru.sadkov.tm.api.ServiceLocator;
import ru.sadkov.tm.entity.Session;
import ru.sadkov.tm.entity.User;
import ru.sadkov.tm.enumeration.Role;
import ru.sadkov.tm.exception.AccessForbiddenException;
import ru.sadkov.tm.repository.SessionRepository;
import ru.sadkov.tm.util.ConnectionUtil;
import ru.sadkov.tm.util.RandomUtil;
import ru.sadkov.tm.util.SignatureUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public final class SessionService implements ISessionService {

    @NotNull
    final private ServiceLocator serviceLocator;

    public SessionService(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public Session open(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        try (@Nullable final Connection connection = ConnectionUtil.getConnection()) {
            if (connection == null) return null;
            @Nullable final ISessionRepository sessionRepository = new SessionRepository(connection);
            @Nullable Session session;
            if (serviceLocator.getUserService().login(login, password)) {
                session = new Session();
                session.setTimeStamp(System.currentTimeMillis());
                @Nullable final User user = serviceLocator.getUserService().findOneByLogin(login);
                if (user == null) return null;
                session.setUserId(user.getId());
                session.setId(RandomUtil.UUID());
                session.setRoles(new Role[]{user.getRole()});
                session = sign(session);
                sessionRepository.persist(session);
                return session;
            }
            return null;
        } catch (SQLException e) {
            e.getMessage();
            return null;
        }
    }

    @Override
    public void validate(@Nullable final Session session) throws Exception {
        if (session == null) throw new Exception();
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessForbiddenException();
        if (session.getUserId() == null || session.getUserId().isEmpty()) throw new AccessForbiddenException();
        if (session.getTimeStamp() == null) throw new AccessForbiddenException();
        if (session.getRoles() == null) throw new AccessForbiddenException();
        @Nullable final Session temp = session.clone();
        if (temp == null) throw new AccessForbiddenException();
        @NotNull final String signatureSource = session.getSignature();
        @NotNull final String signatureTemp = sign(temp).getSignature();
        final boolean check = signatureSource.equals(signatureTemp);
        if (!check) throw new Exception();
        try (@Nullable final Connection connection = ConnectionUtil.getConnection()) {
            if (connection == null) throw new AccessForbiddenException();
            @Nullable final ISessionRepository sessionRepository = new SessionRepository(connection);
            if (!sessionRepository.contains(session)) throw new AccessForbiddenException();
        } catch (SQLException e) {
            e.getMessage();
        }
    }

    @Nullable
    private Session sign(@Nullable final Session session) {
        if (session == null) return null;
        session.setSignature(null);
        @NotNull final String salt = serviceLocator.getPropertyService().getSessionSalt();
        @NotNull final Integer cycle = serviceLocator.getPropertyService().getSessionCycle();
        @Nullable final String signature = SignatureUtil.sign(session, salt, cycle);
        if (signature == null || signature.isEmpty()) return null;
        session.setSignature(signature);
        return session;
    }

    @Override
    public void validate(@Nullable final Session session, @Nullable final Role role) throws Exception {
        if (session == null || role == null) throw new Exception();
        validate(session);
        @NotNull final String userId = session.getUserId();
        @Nullable final User user = serviceLocator.getUserService().findOneById(userId);
        if (user == null) throw new AccessForbiddenException();
        final boolean check = user.getRole().equals(role);
        if (!check) throw new AccessForbiddenException();
    }

    @Override
    public boolean isValid(@Nullable final Session session) {
        try {
            validate(session);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public void close(@Nullable final Session session) {
        try (@Nullable final Connection connection = ConnectionUtil.getConnection()) {
            if (connection == null) return;
            @Nullable final ISessionRepository sessionRepository = new SessionRepository(connection);
            sessionRepository.close(session);
        } catch (SQLException e) {
            e.getMessage();
        }
    }

    @Override
    public void closeAll(@Nullable final Session session) {
        try (@Nullable final Connection connection = ConnectionUtil.getConnection()) {
            if (connection == null) return;
            @Nullable final ISessionRepository sessionRepository = new SessionRepository(connection);
            sessionRepository.clear();
        } catch (SQLException e) {
            e.getMessage();
        }
    }

    @Override
    public List<Session> getListSession(@Nullable final Session session) {
        try (@Nullable final Connection connection = ConnectionUtil.getConnection()) {
            if (connection == null) return new ArrayList<>();
            @Nullable final ISessionRepository sessionRepository = new SessionRepository(connection);
            return sessionRepository.findByUserId(session.getUserId());
        } catch (SQLException e) {
            e.getMessage();
            return new ArrayList<>();
        }
    }

    @Override
    public User getUser(@NotNull final Session session) {
        return serviceLocator.getUserService().findOneById(session.getUserId());
    }

    @Override
    public void load(@Nullable final List<Session> sessions) {
        if (sessions == null) return;
        try (@Nullable final Connection connection = ConnectionUtil.getConnection()) {
            if (connection == null) return;
            @Nullable final ISessionRepository sessionRepository = new SessionRepository(connection);
            sessionRepository.clear();
            for (@NotNull final Session session : sessions) {
                sessionRepository.persist(session);
            }
        } catch (SQLException e) {
            e.getMessage();
        }
    }

    @Override
    @NotNull
    public List<Session> findAll() {
        try (@Nullable final Connection connection = ConnectionUtil.getConnection()) {
            if (connection == null) return new ArrayList<>();
            @Nullable final ISessionRepository sessionRepository = new SessionRepository(connection);
            return sessionRepository.findAll();
        } catch (SQLException e) {
            e.getMessage();
            return new ArrayList<>();
        }
    }
}
