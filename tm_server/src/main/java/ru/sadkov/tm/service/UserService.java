package ru.sadkov.tm.service;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.api.IUserRepository;
import ru.sadkov.tm.api.IUserService;
import ru.sadkov.tm.entity.User;
import ru.sadkov.tm.enumeration.Role;
import ru.sadkov.tm.repository.UserRepository;
import ru.sadkov.tm.util.ConnectionUtil;
import ru.sadkov.tm.util.HashUtil;

import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public final class UserService extends AbstractService implements IUserService {


    public UserService() {
    }

    public void userRegister(@Nullable final User user) {
        if (user == null) return;
        if (user.getLogin() == null || user.getLogin().isEmpty()) return;
        if (user.getPassword() == null || user.getPassword().isEmpty()) return;
        try (@Nullable final Connection connection = ConnectionUtil.getConnection()) {
            if (connection == null) return;
            @NotNull final UserRepository userRepository = new UserRepository(connection);
            if (userRepository.findByLogin(user.getLogin()) != null) return;
            user.setPassword(HashUtil.hashMD5(user.getPassword()));
            userRepository.persist(user);
        } catch (SQLException e) {
            return;
        }
    }

    public void userAddFromData(@Nullable final User user) {
        if (user == null) return;
        try (@Nullable final Connection connection = ConnectionUtil.getConnection()) {
            if (connection == null) return;
            @NotNull final UserRepository userRepository = new UserRepository(connection);
            userRepository.persist(user);
        } catch (SQLException e) {
            return;
        }
    }


    public boolean login(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) return false;
        if (password == null || password.isEmpty()) return false;
        try (@Nullable final Connection connection = ConnectionUtil.getConnection()) {
            if (connection == null) return false;
            @NotNull final UserRepository userRepository = new UserRepository(connection);
            @Nullable final User user = userRepository.findByLogin(login);
            if (user == null || user.getPassword() == null) return false;
            return user.getPassword().equals(HashUtil.hashMD5(password));
        } catch (SQLException e) {
            return false;
        }
    }

    @Override
    @NotNull
    public List<User> findAll() {
        try (@Nullable final Connection connection = ConnectionUtil.getConnection()) {
            if (connection == null) return new ArrayList<>();
            @NotNull final UserRepository userRepository = new UserRepository(connection);
            @NotNull final List<User> userList = userRepository.findAll();
            return userList;
        } catch (SQLException e) {
            return new ArrayList<>();
        }
    }

    public void addTestUsers() {
        @NotNull final User admin = new User("admin", "admin", Role.ADMIN);
        @NotNull final User user = new User("user", "user", Role.USER);
        try (@Nullable final Connection connection = ConnectionUtil.getConnection()) {
            if (connection == null) return;
            @NotNull final UserRepository userRepository = new UserRepository(connection);
            userRepository.persist(admin);
            userRepository.persist(user);
        } catch (SQLException e) {
            e.getMessage();
        }

    }

    @Override
    public void clear() {
        try (@Nullable final Connection connection = ConnectionUtil.getConnection()) {
            if (connection == null) return;
            @NotNull final UserRepository userRepository = new UserRepository(connection);
            userRepository.clear();
        } catch (SQLException e) {
            e.getMessage();
        }

    }

    @Override
    public void load(@Nullable final List<User> users) {
        try (@Nullable final Connection connection = ConnectionUtil.getConnection()) {
            if (connection == null) return;
            @NotNull final UserRepository userRepository = new UserRepository(connection);
            if (users == null) return;
            userRepository.clear();
            for (@NotNull final User user : users) {
                userRepository.persist(user);
            }
        } catch (SQLException e) {
            e.getMessage();
        }
    }

    @Override
    @Nullable
    public User findOneById(@NotNull String userId) {
        try (@Nullable final Connection connection = ConnectionUtil.getConnection()) {
            if (connection == null) return null;
            @NotNull final UserRepository userRepository = new UserRepository(connection);
            @Nullable final User user = userRepository.findOne(userId);
            return user;
        } catch (SQLException e) {
            return null;
        }
    }

    @Override
    @Nullable
    public User findOneByLogin(@Nullable final String login) {
        try (@Nullable final Connection connection = ConnectionUtil.getConnection()) {
            if (connection == null) return null;
            @NotNull final UserRepository userRepository = new UserRepository(connection);
            if (login == null || login.isEmpty()) return null;
            @Nullable final User user = userRepository.findByLogin(login);
            return user;
        } catch (SQLException e) {
            return null;
        }
    }
}
