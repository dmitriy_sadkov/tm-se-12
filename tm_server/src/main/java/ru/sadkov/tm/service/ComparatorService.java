package ru.sadkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.comparator.*;
import ru.sadkov.tm.entity.AbstractEntity;

import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;

public final class ComparatorService {
    @NotNull private final Map<String, Comparator<? extends AbstractEntity>> comparatorMap = new LinkedHashMap<>();

    public ComparatorService() {
        comparatorMap.put("projectDateStart",new ProjectDateStartComparator());
        comparatorMap.put("projectDateEnd",new ProjectDateEndComparator());
        comparatorMap.put("projectDateCreate", new ProjectDateCreateComparator());
        comparatorMap.put("projectStatus",new ProjectStatusComparator());
        comparatorMap.put("taskDateStart",new TaskDateStartComparator());
        comparatorMap.put("taskDateEnd",new TaskDateEndComparator());
        comparatorMap.put("taskDateCreate", new TaskDateCreateComparator());
        comparatorMap.put("taskStatus",new TaskStatusComparator());
    }

    public Comparator<? extends AbstractEntity> getComparator(@Nullable final String comparator){
        if(comparator==null||comparator.isEmpty()) return null;
        return comparatorMap.get(comparator);
    }
}
