package ru.sadkov.tm.service;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;

public abstract class AbstractService {

    @NotNull
    protected SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
}
