package ru.sadkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.api.IProjectRepository;
import ru.sadkov.tm.api.IProjectService;
import ru.sadkov.tm.entity.AbstractEntity;
import ru.sadkov.tm.entity.Project;
import ru.sadkov.tm.entity.User;
import ru.sadkov.tm.enumeration.Status;
import ru.sadkov.tm.repository.ProjectRepository;
import ru.sadkov.tm.util.ConnectionUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public final class ProjectService extends AbstractService implements IProjectService {

    public ProjectService() {
    }

    @Override
    public List<Project> getSortedProjectList(@Nullable final String userId, @Nullable final Comparator<? extends AbstractEntity> comparator) {
        if (userId == null || userId.isEmpty()) return null;
        if (comparator == null) return null;
        try (@Nullable final Connection connection = ConnectionUtil.getConnection()) {
            if (connection == null) return null;
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
            @NotNull final List<Project> projects = new ArrayList<>(projectRepository.findAll(userId));
            @Nullable final Comparator<Project> comparator1 = (Comparator<Project>) comparator;
            projects.sort(comparator1);
            return projects;
        } catch (SQLException e) {
            return null;
        }

    }

    @Nullable
    public String findProjectIdByName(@Nullable final String projectName, @Nullable final String userId) {
        if (projectName == null || projectName.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        try (@Nullable final Connection connection = ConnectionUtil.getConnection()) {
            if (connection == null) return null;
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
            return projectRepository.findProjectByName(projectName, userId).getId();
        } catch (SQLException e) {
            return null;
        }

    }

    public boolean persist(@Nullable final String projectName, @Nullable final String userId, @Nullable final String description) {
        if (projectName == null || projectName.isEmpty()) return false;
        if (userId == null || userId.isEmpty()) return false;
        if (description == null || description.isEmpty()) return false;
        try (@Nullable final Connection connection = ConnectionUtil.getConnection()) {
            if (connection == null) return false;
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
            return projectRepository.merge(projectName, userId, description);
        } catch (SQLException e) {
            return false;
        }

    }

    public void removeByName(@Nullable final String projectName, @Nullable final String userId) {
        if (projectName == null || projectName.isEmpty() || userId == null || userId.isEmpty()) return;
        @Nullable final String projectId = findProjectIdByName(projectName, userId);
        if (projectId == null || projectId.isEmpty()) return;
        try (@Nullable final Connection connection = ConnectionUtil.getConnection()) {
            if (connection == null) return;
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
            projectRepository.remove(userId, projectName);
        } catch (SQLException e) {
            e.getMessage();
        }

    }

    @Nullable
    public List<Project> findAll(@Nullable final User user) {
        if (user == null) return null;
        try (@Nullable final Connection connection = ConnectionUtil.getConnection()) {
            if (connection == null) return null;
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
            return new ArrayList<>(projectRepository.findAll(user.getId()));
        } catch (SQLException e) {
            return null;
        }
    }

    public void removeAll(@Nullable final User user) {
        if (user == null) return;
        try (@Nullable final Connection connection = ConnectionUtil.getConnection()) {
            if (connection == null) return;
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
            projectRepository.removeAll(user.getId());
        } catch (SQLException e) {
            e.getMessage();
        }
    }

    public void update(@Nullable final Project project, @Nullable final String projectName, @Nullable final String description) {
        if (project == null || projectName == null || projectName.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        try (@Nullable final Connection connection = ConnectionUtil.getConnection()) {
            if (connection == null) return;
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
            projectRepository.update(project.getUserId(), project.getId(), projectName, description);
        } catch (SQLException e) {
            e.getMessage();
        }

    }

    @Nullable
    public Project findOneByName(@Nullable final String projectName, @Nullable final User currentUser) {
        if (projectName == null || projectName.isEmpty() || currentUser == null) return null;
        @Nullable final String projectId = findProjectIdByName(projectName, currentUser.getId());
        if (projectId == null || projectId.isEmpty()) return null;
        try (@Nullable final Connection connection = ConnectionUtil.getConnection()) {
            if (connection == null) return null;
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
            return projectRepository.findOne(projectId, currentUser.getId());
        } catch (SQLException e) {
            return null;
        }
    }

    @Override
    @Nullable
    public List<Project> findProjectsByPart(@Nullable final String userId, @Nullable final String part) {
        if (userId == null || userId.isEmpty()) return null;
        if (part == null || part.isEmpty()) return null;
        try (@Nullable final Connection connection = ConnectionUtil.getConnection()) {
            if (connection == null) return null;
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
            return projectRepository.findProjectsByPart(userId, part);
        } catch (SQLException e) {
            return null;
        }
    }

    @Override
    @Nullable
    public List<Project> findProjectsByStatus(@Nullable final String userId, @Nullable final Status status) {
        if (userId == null || userId.isEmpty() || status == null) return null;
        try (@Nullable final Connection connection = ConnectionUtil.getConnection()) {
            if (connection == null) return null;
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
            return projectRepository.findProjectsByStatus(userId, status);
        } catch (SQLException e) {
            return null;
        }
    }

    @Override
    @Nullable
    public String startProject(@Nullable final String userId, @Nullable final String projectName) {
        if (userId == null || userId.isEmpty()) return null;
        if (projectName == null || projectName.isEmpty()) return null;
        try (@Nullable final Connection connection = ConnectionUtil.getConnection()) {
            if (connection == null) return null;
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
            @Nullable final Date startDate = projectRepository.startProject(userId, projectName);
            if (startDate == null) return null;
            return simpleDateFormat.format(startDate);
        } catch (SQLException e) {
            return null;
        }

    }

    @Override
    public @Nullable String endProject(@Nullable final String userId, @Nullable final String projectName) {
        if (userId == null || userId.isEmpty()) return null;
        if (projectName == null || projectName.isEmpty()) return null;
        try (@Nullable final Connection connection = ConnectionUtil.getConnection()) {
            if (connection == null) return null;
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
            @Nullable final Date endDate = projectRepository.endProject(userId, projectName);
            if (endDate == null) return null;
            return simpleDateFormat.format(endDate);
        } catch (SQLException e) {
            return null;
        }
    }

    @Override
    @NotNull
    public List<Project> findAll(@NotNull final List<User> userList) {
        try (@Nullable final Connection connection = ConnectionUtil.getConnection()) {
            if (connection == null) return new ArrayList<>();
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
            return projectRepository.findAll(userList);
        } catch (SQLException e) {
            return new ArrayList<>();
        }
    }

    @Override
    public void persist(@Nullable final Project project) {
        if (project == null) return;
        try (@Nullable final Connection connection = ConnectionUtil.getConnection()) {
            if (connection == null) return;
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
            projectRepository.persist(project);
        } catch (SQLException e) {
            e.getMessage();
        }
    }

    @Override
    public void clear() {
        try (@Nullable final Connection connection = ConnectionUtil.getConnection()) {
            if (connection == null) return;
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
            projectRepository.clear();
        } catch (SQLException e) {
            e.getMessage();
        }
    }

    @Override
    public void load(@Nullable final List<Project> projects) {
        if (projects == null) return;
        clear();
        try (@Nullable final Connection connection = ConnectionUtil.getConnection()) {
            if (connection == null) return;
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
            for (@NotNull final Project project : projects) {
                projectRepository.persist(project);
            }
        } catch (SQLException e) {
            e.getMessage();
        }
    }
}
