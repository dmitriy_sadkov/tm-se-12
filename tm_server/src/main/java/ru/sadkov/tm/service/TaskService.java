package ru.sadkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.api.IProjectService;
import ru.sadkov.tm.api.ITaskRepository;
import ru.sadkov.tm.api.ITaskService;
import ru.sadkov.tm.entity.AbstractEntity;
import ru.sadkov.tm.entity.Project;
import ru.sadkov.tm.entity.Task;
import ru.sadkov.tm.entity.User;
import ru.sadkov.tm.enumeration.Status;
import ru.sadkov.tm.repository.TaskRepository;
import ru.sadkov.tm.util.ConnectionUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;

public final class TaskService extends AbstractService implements ITaskService {

    @NotNull
    private IProjectService projectService;

    public TaskService(@NotNull final IProjectService projectService) {
        this.projectService = projectService;
    }

    @Nullable
    public Task findTaskByName(@Nullable final String taskName, @Nullable final String userId) {
        if (taskName == null || taskName.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        try (@Nullable final Connection connection = ConnectionUtil.getConnection()) {
            if (connection == null) return null;
            @NotNull TaskRepository taskRepository = new TaskRepository(connection);
            @NotNull final List<Task> tasks = taskRepository.findAll(userId);
            for (@NotNull final Task task : tasks) {
                if (task.getName().equals(taskName)) {
                    return task;
                }
            }
            return null;
        } catch (SQLException e) {
            return null;
        }
    }

    @Nullable
    public List<Task> getSortedTaskList(@Nullable final String userId, @Nullable final Comparator<? extends AbstractEntity> comparator) {
        if (userId == null || userId.isEmpty()) return null;
        if (comparator == null) return null;
        try (@Nullable final Connection connection = ConnectionUtil.getConnection()) {
            if (connection == null) return null;
            @NotNull TaskRepository taskRepository = new TaskRepository(connection);
            @NotNull final List<Task> tasks = taskRepository.findAll(userId);
            @Nullable final Comparator<Task> comparator1 = (Comparator<Task>) comparator;
            tasks.sort(comparator1);
            return tasks;
        } catch (SQLException e) {
            return null;
        }
    }

    public boolean saveTask(@Nullable final String taskName, @Nullable final String projectName, @Nullable final User currentUser) {
        if (taskName == null || taskName.isEmpty()) return false;
        if (projectName == null || projectName.isEmpty()) return false;
        if (currentUser == null) return false;
        @Nullable final String projectId = projectService.findProjectIdByName(projectName, currentUser.getId());
        if (projectId == null || projectId.isEmpty()) return false;
        try (@Nullable final Connection connection = ConnectionUtil.getConnection()) {
            if (connection == null) return false;
            @NotNull TaskRepository taskRepository = new TaskRepository(connection);
            taskRepository.merge(taskName, projectId, currentUser.getId());
            return true;
        } catch (SQLException e) {
            return false;
        }
    }

    public void removeTask(@Nullable final String taskName, @Nullable final User currentUser) {
        if (taskName == null || taskName.isEmpty()) return;
        if (currentUser == null) return;
        try (@Nullable final Connection connection = ConnectionUtil.getConnection()) {
            if (connection == null) return;
            @NotNull TaskRepository taskRepository = new TaskRepository(connection);
            @Nullable final Task task = taskRepository.findTaskByName(taskName, currentUser.getId());
            if (task == null) return;
            taskRepository.removeByName(taskName, currentUser.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Nullable
    public List<Task> findAll(@Nullable final User user) {
        if (user == null) return null;
        try (@Nullable final Connection connection = ConnectionUtil.getConnection()) {
            if (connection == null) return null;
            @NotNull TaskRepository taskRepository = new TaskRepository(connection);
            return taskRepository.findAll(user.getId());
        } catch (SQLException e) {
            return null;
        }
    }


    public void removeAll(@Nullable final User user) {
        if (user == null) return;
        try (@Nullable final Connection connection = ConnectionUtil.getConnection()) {
            if (connection == null) return;
            @NotNull TaskRepository taskRepository = new TaskRepository(connection);
            taskRepository.removeAll(user.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void removeTaskForProject(@Nullable final String projectName, @Nullable final User currentUser) {
        if (projectName == null || projectName.isEmpty() || currentUser == null) return;
        @Nullable final String projectId = projectService.findProjectIdByName(projectName, currentUser.getId());
        if (projectId == null || projectId.isEmpty()) return;
        try (@Nullable final Connection connection = ConnectionUtil.getConnection()) {
            if (connection == null) return;
            @NotNull TaskRepository taskRepository = new TaskRepository(connection);
            @Nullable final Project project = projectService.findOneByName(projectName, currentUser);
            if (project == null) return;
            if (project.getUserId().equals(currentUser.getId())) {
                @NotNull final Iterator<Task> iterator = taskRepository.findAll(currentUser.getId()).iterator();
                while (iterator.hasNext()) {
                    @NotNull final Task task = iterator.next();
                    if (task.getProjectId().equals(projectId)) {
                        taskRepository.removeByName(task.getName(), currentUser.getId());
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void update(@Nullable final String oldName, @Nullable final String newName, @Nullable final String userId) {
        if (oldName == null || oldName.isEmpty()) return;
        if (newName == null || newName.isEmpty()) return;
        if (userId == null || userId.isEmpty()) return;
        try (@Nullable final Connection connection = ConnectionUtil.getConnection()) {
            if (connection == null) return;
            @NotNull TaskRepository taskRepository = new TaskRepository(connection);
            taskRepository.update(oldName, newName, userId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    @Nullable
    public List<Task> getTasksByPart(@Nullable final String userId, @Nullable final String part) {
        if (userId == null || userId.isEmpty()) return null;
        if (part == null || part.isEmpty()) return null;
        try (@Nullable final Connection connection = ConnectionUtil.getConnection()) {
            if (connection == null) return null;
            @NotNull TaskRepository taskRepository = new TaskRepository(connection);
            return taskRepository.getTasksByPart(userId, part);
        } catch (SQLException e) {
            return null;
        }
    }

    @Override
    @Nullable
    public List<Task> findTasksByStatus(@Nullable final String userId, @Nullable final Status status) {
        if (userId == null || userId.isEmpty() || status == null) return null;
        try (@Nullable final Connection connection = ConnectionUtil.getConnection()) {
            if (connection == null) return null;
            @NotNull TaskRepository taskRepository = new TaskRepository(connection);
            return taskRepository.findTasksByStatus(userId, status);
        } catch (SQLException e) {
            return null;
        }
    }

    @Override
    @Nullable
    public String startTask(@Nullable final String userId, @Nullable final String taskName) {
        if (userId == null || userId.isEmpty()) return null;
        if (taskName == null || taskName.isEmpty()) return null;
        try (@Nullable final Connection connection = ConnectionUtil.getConnection()) {
            if (connection == null) return null;
            @NotNull TaskRepository taskRepository = new TaskRepository(connection);
            @Nullable final Date startDate = taskRepository.startTask(userId, taskName);
            if (startDate == null) return null;
            return simpleDateFormat.format(startDate);
        } catch (SQLException e) {
            return null;
        }
    }

    @Override
    @Nullable
    public String endTask(@Nullable final String userId, @Nullable final String taskName) {
        if (userId == null || userId.isEmpty()) return null;
        if (taskName == null || taskName.isEmpty()) return null;
        try (@Nullable final Connection connection = ConnectionUtil.getConnection()) {
            if (connection == null) return null;
            @NotNull TaskRepository taskRepository = new TaskRepository(connection);
            @Nullable final Date endDate = taskRepository.endTask(userId, taskName);
            if (endDate == null) return null;
            return simpleDateFormat.format(endDate);
        } catch (SQLException e) {
            return null;
        }
    }

    @Override
    @NotNull
    public List<Task> findAll(@NotNull final List<User> userList) {
        try (@Nullable final Connection connection = ConnectionUtil.getConnection()) {
            if (connection == null) return null;
            @NotNull TaskRepository taskRepository = new TaskRepository(connection);
            return taskRepository.findAll(userList);
        } catch (SQLException e) {
            return new ArrayList<>();
        }
    }

    @Override
    public void persist(@Nullable final Task task) {
        if (task == null) return;
        try (@Nullable final Connection connection = ConnectionUtil.getConnection()) {
            if (connection == null) return;
            @NotNull TaskRepository taskRepository = new TaskRepository(connection);
            taskRepository.persist(task);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void clear() {
        try (@Nullable final Connection connection = ConnectionUtil.getConnection()) {
            if (connection == null) return;
            @NotNull TaskRepository taskRepository = new TaskRepository(connection);
            taskRepository.clear();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void load(@Nullable final List<Task> tasks) {
        if (tasks == null) return;
        clear();
        try (@Nullable final Connection connection = ConnectionUtil.getConnection()) {
            if (connection == null) return;
            @NotNull TaskRepository taskRepository = new TaskRepository(connection);
            for (@NotNull final Task task : tasks) {
                taskRepository.persist(task);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

