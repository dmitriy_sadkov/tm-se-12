package ru.sadkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.api.ITaskRepository;
import ru.sadkov.tm.entity.Task;
import ru.sadkov.tm.entity.User;
import ru.sadkov.tm.enumeration.Status;
import ru.sadkov.tm.util.RandomUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @NotNull
    private final Connection connection;

    public TaskRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }


    @NotNull
    public List<Task> findAll(@NotNull final String userId) throws SQLException {
        @NotNull final List<Task> result = new ArrayList<>();
        @NotNull final String query = "select * from task where user_id = ?";
        @Nullable final PreparedStatement statement = connection.prepareStatement(query);
        if (statement == null) return result;
        statement.setString(1, userId);
        @Nullable ResultSet resultSet = statement.executeQuery();
        if (resultSet == null) return result;
        while (resultSet.next()) {
            @NotNull final Task task = new Task();
            task.setId(resultSet.getString("id"));
            task.setName(resultSet.getString("name"));
            task.setDescription(resultSet.getString("description"));
            task.setUserId(resultSet.getString("user_id"));
            task.setStatus(Status.valueOf(resultSet.getString("status")));
            task.setDateCreate(resultSet.getDate("date_create"));
            task.setDateBegin(resultSet.getDate("date_begin"));
            task.setDateEnd(resultSet.getDate("date_end"));
            task.setProjectId(resultSet.getString("project_id"));
            result.add(task);
        }
        return result;
    }

    public void persist(@NotNull final Task task) throws SQLException {
        @NotNull final String query = "INSERT INTO task (id, name, description, date_create, date_begin, date_end, project_id, user_id, status) " +
                "VALUES(?,?,?,?,?,?,?,?,?)";
        @Nullable final PreparedStatement preparedStatement = connection.prepareStatement(query);
        if (preparedStatement == null) return;
        preparedStatement.setString(1, task.getId());
        preparedStatement.setString(2, task.getName());
        preparedStatement.setString(3, task.getDescription());
        preparedStatement.setString(4, simpleDateFormat.format(task.getDateCreate()));
        preparedStatement.setString(5, simpleDateFormat.format(task.getDateBegin()));
        preparedStatement.setString(6, simpleDateFormat.format(task.getDateEnd()));
        preparedStatement.setString(7, task.getProjectId());
        preparedStatement.setString(8, task.getUserId());
        preparedStatement.setString(9, task.getStatus().toString());
        preparedStatement.executeUpdate();
    }

    public void removeByName(@NotNull final String taskName, @NotNull final String userId) throws SQLException {
        @NotNull final String query = "delete from task where user_id = ? and name = ?";
        @Nullable final PreparedStatement preparedStatement = connection.prepareStatement(query);
        if (preparedStatement == null) return;
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, taskName);
        preparedStatement.executeUpdate();
    }

    public void removeAll(@NotNull final String userId) throws SQLException {
        @NotNull final String query = "delete from task where user_id = ?";
        @Nullable final PreparedStatement preparedStatement = connection.prepareStatement(query);
        if (preparedStatement == null) return;
        preparedStatement.setString(1, userId);
        preparedStatement.executeUpdate();
    }

    @Nullable
    public Task findOne(@NotNull final String taskId, @NotNull final String userId) throws SQLException {
        @NotNull final String query = "select * from task where user_id = ? and id = ?";
        @Nullable final PreparedStatement statement = connection.prepareStatement(query);
        if (statement == null) return null;
        statement.setString(1, userId);
        statement.setString(2, taskId);
        @Nullable ResultSet resultSet = statement.executeQuery();
        if (resultSet == null) return null;
        while (resultSet.next()) {
            @NotNull final Task task = new Task();
            task.setId(resultSet.getString("id"));
            task.setName(resultSet.getString("name"));
            task.setDescription(resultSet.getString("description"));
            task.setUserId(resultSet.getString("user_id"));
            task.setStatus(Status.valueOf(resultSet.getString("status")));
            task.setDateCreate(resultSet.getDate("date_create"));
            task.setDateBegin(resultSet.getDate("date_begin"));
            task.setDateEnd(resultSet.getDate("date_end"));
            task.setProjectId(resultSet.getString("project_id"));
            return task;
        }
        return null;
    }

    public void merge(@NotNull final String taskName, @NotNull final String projectId, @NotNull final String userId) throws SQLException {
        @Nullable final Task task = findTaskByName(taskName, userId);
        if (task == null) {
            persist(new Task(taskName, RandomUtil.UUID(), projectId, userId));
            return;
        }
        update(taskName, taskName, userId);
    }

    public void update(@NotNull final String oldName, @NotNull final String newName, @NotNull final String userId) throws SQLException {
        @Nullable final Task task = findTaskByName(oldName, userId);
        if (task == null) return;
        @NotNull final String query = "update task set name = ? where user_id = ? and id = ?";
        @Nullable final PreparedStatement statement = connection.prepareStatement(query);
        if (statement == null) return;
        statement.setString(1, newName);
        statement.setString(2, userId);
        statement.setString(3, task.getId());
        statement.executeUpdate(query);
    }

    @Nullable
    public Task findTaskByName(@NotNull final String taskName, @NotNull final String userId) throws SQLException {
        @NotNull final String query = "select * from task where user_id = ? and name = ?";
        @Nullable final PreparedStatement statement = connection.prepareStatement(query);
        if (statement == null) return null;
        statement.setString(1, userId);
        statement.setString(2, taskName);
        @Nullable ResultSet resultSet = statement.executeQuery();
        if (resultSet == null) return null;
        while (resultSet.next()) {
            @NotNull final Task task = new Task();
            task.setId(resultSet.getString("id"));
            task.setName(resultSet.getString("name"));
            task.setDescription(resultSet.getString("description"));
            task.setUserId(resultSet.getString("user_id"));
            task.setStatus(Status.valueOf(resultSet.getString("status")));
            task.setDateCreate(resultSet.getDate("date_create"));
            task.setDateBegin(resultSet.getDate("date_begin"));
            task.setDateEnd(resultSet.getDate("date_end"));
            task.setProjectId(resultSet.getString("project_id"));
            return task;
        }
        return null;
    }

    @Override
    @NotNull
    public List<Task> getTasksByPart(@NotNull final String userId, @NotNull final String part) throws SQLException {
        @NotNull final List<Task> tasks = new ArrayList<>();
        @NotNull final String query = "select * from task where user_id = ? and name like '%?%' or description like '%?%'";
        @Nullable final PreparedStatement statement = connection.prepareStatement(query);
        if (statement == null) return tasks;
        statement.setString(1, userId);
        statement.setString(2, part);
        statement.setString(3, part);
        @Nullable ResultSet resultSet = statement.executeQuery();
        if (resultSet == null) return tasks;
        while (resultSet.next()) {
            @NotNull final Task task = new Task();
            task.setId(resultSet.getString("id"));
            task.setName(resultSet.getString("name"));
            task.setDescription(resultSet.getString("description"));
            task.setUserId(resultSet.getString("user_id"));
            task.setStatus(Status.valueOf(resultSet.getString("status")));
            task.setDateCreate(resultSet.getDate("date_create"));
            task.setDateBegin(resultSet.getDate("date_begin"));
            task.setDateEnd(resultSet.getDate("date_end"));
            task.setProjectId(resultSet.getString("project_id"));
            tasks.add(task);
        }
        return tasks;
    }

    @Override
    @NotNull
    public List<Task> findTasksByStatus(@NotNull final String userId, @NotNull final Status status) throws SQLException {
        @NotNull final List<Task> tasks = new ArrayList<>();
        @NotNull final String query = "select * from task where user_id = ? and status = ?";
        @Nullable final PreparedStatement statement = connection.prepareStatement(query);
        if (statement == null) return tasks;
        statement.setString(1, userId);
        statement.setString(2, status.getDisplayName());
        @Nullable ResultSet resultSet = statement.executeQuery();
        if (resultSet == null) return tasks;
        while (resultSet.next()) {
            @NotNull final Task task = new Task();
            task.setId(resultSet.getString("id"));
            task.setName(resultSet.getString("name"));
            task.setDescription(resultSet.getString("description"));
            task.setUserId(resultSet.getString("user_id"));
            task.setStatus(Status.valueOf(resultSet.getString("status")));
            task.setDateCreate(resultSet.getDate("date_create"));
            task.setDateBegin(resultSet.getDate("date_begin"));
            task.setDateEnd(resultSet.getDate("date_end"));
            task.setProjectId(resultSet.getString("project_id"));
            tasks.add(task);
        }
        return tasks;
    }

    @Override
    @Nullable
    public Date startTask(@NotNull final String userId, @NotNull final String taskName) throws SQLException {
        @NotNull final Date startDate = new Date();
        @Nullable final Task task = findTaskByName(taskName, userId);
        if (task == null || !task.getStatus().equals(Status.PLANNED)) return null;
        @NotNull final String query = "update task set status = ? and date_begin = ? where user_id = ? and name = ?;";
        @Nullable final PreparedStatement statement = connection.prepareStatement(query);
        if (statement == null) return null;
        statement.setString(1, Status.PROCESS.getDisplayName());
        statement.setDate(2, (java.sql.Date) startDate);
        statement.setString(3, userId);
        statement.setString(4, taskName);
        statement.executeUpdate();
        return startDate;
    }

    @Override
    @Nullable
    public Date endTask(@NotNull final String userId, @NotNull final String taskName) throws SQLException {
        @NotNull final Date endDate = new Date();
        @Nullable final Task task = findTaskByName(taskName, userId);
        if (task == null || !task.getStatus().equals(Status.PROCESS)) return null;
        @NotNull final String query = "update task set status = ? and date_end = ? where user_id = ? and name = ?;";
        @Nullable final PreparedStatement statement = connection.prepareStatement(query);
        if (statement == null) return null;
        statement.setString(1, Status.PROCESS.getDisplayName());
        statement.setDate(2, (java.sql.Date) endDate);
        statement.setString(3, userId);
        statement.setString(4, taskName);
        statement.executeUpdate();
        return endDate;
    }

    @Override
    @NotNull
    public List<Task> findAll(@NotNull List<User> userList) throws SQLException {
        @NotNull final List<Task> result = new ArrayList<>();
        @NotNull final String query = "select * from task";
        @Nullable final PreparedStatement statement = connection.prepareStatement(query);
        if (statement == null) return result;
        @Nullable ResultSet resultSet = statement.executeQuery();
        if (resultSet == null) return result;
        while (resultSet.next()) {
            @NotNull final Task task = new Task();
            task.setId(resultSet.getString("id"));
            task.setName(resultSet.getString("name"));
            task.setDescription(resultSet.getString("description"));
            task.setUserId(resultSet.getString("user_id"));
            task.setStatus(Status.valueOf(resultSet.getString("status")));
            task.setDateCreate(resultSet.getDate("date_create"));
            task.setDateBegin(resultSet.getDate("date_begin"));
            task.setDateEnd(resultSet.getDate("date_end"));
            task.setProjectId(resultSet.getString("project_id"));
            result.add(task);
        }
        return result;
    }

    @Override
    public void clear() throws SQLException {
        @Nullable final Statement statement = connection.createStatement();
        if (statement == null) return;
        @NotNull final String query = "delete from task;";
        statement.executeUpdate(query);
    }
}
