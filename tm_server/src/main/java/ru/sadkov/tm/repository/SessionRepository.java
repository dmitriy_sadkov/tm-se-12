package ru.sadkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.api.ISessionRepository;
import ru.sadkov.tm.entity.Session;
import ru.sadkov.tm.entity.User;
import ru.sadkov.tm.enumeration.Role;
import ru.sadkov.tm.util.ConnectionUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public final class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    @NotNull
    private final Connection connection;

    public SessionRepository(@NotNull Connection connection) {
        this.connection = connection;
    }

    @Override
    public void persist(@NotNull final Session session) throws SQLException {
        @NotNull final String query = "INSERT INTO session (id, user_id, timestamp, role, signature) VALUES(?,?,?,?,?)";
        @Nullable final PreparedStatement statement = connection.prepareStatement(query);
        if (statement == null) return;
        @NotNull final String id = session.getId();
        @Nullable final String userId = session.getUserId();
        @Nullable final String signature = session.getSignature();
        @Nullable final Long timestamp = session.getTimeStamp();
        @NotNull final Role[] roles = session.getRoles();
        @NotNull final StringBuilder role = new StringBuilder();
        for (@NotNull final Role r : roles) {
            role.append(" ").append(r.getDisplayName());
        }
        statement.setString(1, id);
        statement.setString(2, userId);
        statement.setLong(3, timestamp);
        statement.setString(4, role.toString());
        statement.setString(5, signature);
        statement.executeUpdate();
    }

    @Override
    public void clear() throws SQLException {
        @Nullable final Statement statement = connection.createStatement();
        if (statement == null) return;
        @NotNull final String query = "delete from session;";
        statement.executeUpdate(query);

    }

    @Override
    public boolean isEmpty() throws SQLException {
        @Nullable final Statement statement = connection.createStatement();
        if (statement == null) return false;
        @NotNull final String query = "select count(*) from session;";
        @Nullable final ResultSet result = statement.executeQuery(query);
        if (result == null) return false;
        if (result.next()) {
            final int size = result.getInt(1);
            return size == 0;
        }
        return false;
    }

    @Override
    @NotNull
    public List<Session> findByUserId(@NotNull final String userId) throws SQLException {
        List<Session> sessions = new ArrayList<>();
        @NotNull final String query = "select * from session where user_id = ?";
        @Nullable final PreparedStatement statement = connection.prepareStatement(query);
        if (statement == null) return sessions;
        statement.setString(1, userId);
        @Nullable ResultSet resultSet = statement.executeQuery();
        if (resultSet == null) return sessions;
        while (resultSet.next()) {
            @NotNull final Session session = new Session();
            session.setId(resultSet.getString("id"));
            session.setUserId(resultSet.getString("user_id"));
            session.setSignature(resultSet.getString("signature"));
            session.setTimeStamp(resultSet.getLong("timestamp"));
            @Nullable final String roles = resultSet.getString("role");
            if (roles == null || roles.isEmpty()) {
                session.setRoles(null);
                sessions.add(session);
                continue;
            }
            @NotNull final String[] sessionRoles = roles.trim().split(" ");
            @NotNull final Role[] roles1 = new Role[sessionRoles.length];
            int i = 0;
            for (@NotNull final String s : sessionRoles) {
                roles1[i] = Role.valueOf(s);
            }
            session.setRoles(roles1);
        }
        return sessions;
    }

    @Override
    public boolean contains(final Session session) throws SQLException {
        @NotNull final String query = "select count('id') from session where id = ?";
        @Nullable final PreparedStatement statement = connection.prepareStatement(query);
        if (statement == null) return false;
        statement.setString(1, session.getId());
        @Nullable final ResultSet result = statement.executeQuery();
        if (result == null) return false;
        final int size = result.getInt(1);
        if (size > 0) return true;
        return false;
    }

    @Override
    public void close(@NotNull final Session session) throws SQLException {
        @NotNull final String query = "delete from session where id = ?";
        @Nullable final PreparedStatement statement = connection.prepareStatement(query);
        if (statement == null) return;
        statement.setString(1, session.getId());
        statement.executeUpdate();
    }

    @Override
    public List<Session> findAll() throws SQLException {
        List<Session> sessions = new ArrayList<>();
        @Nullable final Statement statement = connection.createStatement();
        if (statement == null) return sessions;
        @NotNull final String query = "select * from session;";
        @Nullable ResultSet resultSet = statement.executeQuery(query);
        if (resultSet == null) return sessions;
        while (resultSet.next()) {
            @NotNull final Session session = new Session();
            session.setId(resultSet.getString("id"));
            session.setUserId(resultSet.getString("user_id"));
            session.setSignature(resultSet.getString("signature"));
            session.setTimeStamp(resultSet.getLong("timestamp"));
            @Nullable final String roles = resultSet.getString("role");
            if (roles == null || roles.isEmpty()) {
                session.setRoles(null);
                sessions.add(session);
                continue;
            }
            @NotNull final String[] sessionRoles = roles.trim().split(" ");
            @NotNull final Role[] roles1 = new Role[sessionRoles.length];
            int i = 0;
            for (@NotNull final String s : sessionRoles) {
                roles1[i] = Role.valueOf(s);
            }
            session.setRoles(roles1);
        }
        return sessions;
    }
}
