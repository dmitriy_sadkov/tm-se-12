package ru.sadkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.api.IProjectRepository;
import ru.sadkov.tm.entity.Project;
import ru.sadkov.tm.entity.User;
import ru.sadkov.tm.enumeration.Status;
import ru.sadkov.tm.util.RandomUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @NotNull
    private final Connection connection;

    public ProjectRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }


    public void persist(@NotNull final Project project) throws SQLException {
        @NotNull final String query = "INSERT INTO project (id, name, description, date_create, date_begin, date_end, user_id, status) " +
                "VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
        @Nullable final PreparedStatement statement = connection.prepareStatement(query);
        if (statement == null) return;
        @NotNull final String id = project.getId();
        @Nullable final String name = project.getName();
        @Nullable final String description = project.getDescription();
        @Nullable final String status = project.getStatus().toString();
        @Nullable final String userId = project.getUserId();
        @Nullable final String dateCreate = simpleDateFormat.format(project.getDateCreate());
        @Nullable final String dateBegin = simpleDateFormat.format(project.getDateBegin());
        @Nullable final String dateEnd = simpleDateFormat.format(project.getDateEnd());
        statement.setString(1, id);
        statement.setString(2, name);
        statement.setString(3, description);
        statement.setString(4, dateCreate);
        statement.setString(5, dateBegin);
        statement.setString(6, dateEnd);
        statement.setString(7, userId);
        statement.setString(8, status);
        statement.executeUpdate();
    }

    public void remove(@NotNull final String userId, @NotNull final String projectName) throws SQLException {
        @NotNull final String query = "delete from project where user_id = ? and name = ?";
        @Nullable final PreparedStatement statement = connection.prepareStatement(query);
        if (statement == null) return;
        statement.setString(1, userId);
        statement.setString(2, projectName);
        statement.executeUpdate();
    }

    @NotNull
    @Override
    public Collection<Project> findAll(@NotNull final String userId) throws SQLException {
        @NotNull final List<Project> projects = new ArrayList<>();
        @NotNull final String query = "select * from project where user_id = ?";
        @Nullable final PreparedStatement statement = connection.prepareStatement(query);
        if (statement == null) return projects;
        statement.setString(1, userId);
        @Nullable ResultSet resultSet = statement.executeQuery();
        if (resultSet == null) return projects;
        while (resultSet.next()) {
            @NotNull final Project project = new Project();
            project.setId(resultSet.getString("id"));
            project.setName(resultSet.getString("name"));
            project.setDescription(resultSet.getString("description"));
            project.setUserId(resultSet.getString("user_id"));
            project.setStatus(Status.valueOf(resultSet.getString("status")));
            project.setDateCreate(resultSet.getDate("date_create"));
            project.setDateBegin(resultSet.getDate("date_begin"));
            project.setDateEnd(resultSet.getDate("date_end"));
            projects.add(project);
        }
        return projects;
    }

    @Override
    @Nullable
    public Project findProjectByName(@NotNull final String name, @NotNull final String userId) throws SQLException {
        @NotNull final String query = "select * from project where user_id = ? and name = ?";
        @Nullable final PreparedStatement statement = connection.prepareStatement(query);
        if (statement == null) return null;
        statement.setString(1, userId);
        statement.setString(2, name);
        @Nullable ResultSet resultSet = statement.executeQuery();
        if (resultSet == null) return null;
        @NotNull final Project project = new Project();
        while (resultSet.next()) {
            project.setId(resultSet.getString("id"));
            project.setName(resultSet.getString("name"));
            project.setDescription(resultSet.getString("description"));
            project.setUserId(resultSet.getString("user_id"));
            project.setStatus(Status.valueOf(resultSet.getString("status")));
            project.setDateCreate(resultSet.getDate("date_create"));
            project.setDateBegin(resultSet.getDate("date_begin"));
            project.setDateEnd(resultSet.getDate("date_end"));
            return project;
        }
        return null;
    }

    public void removeAll(@NotNull final String userId) throws SQLException {
        @NotNull final String query = "delete from project where user_id = ?";
        @Nullable final PreparedStatement statement = connection.prepareStatement(query);
        if (statement == null) return;
        statement.setString(1, userId);
        statement.executeUpdate();
    }

    @Nullable
    public Project findOne(@NotNull final String projectId, @NotNull final String userId) throws SQLException {
        @NotNull final String query = "select * from project where user_id= ? and id = ?";
        @Nullable final PreparedStatement statement = connection.prepareStatement(query);
        if (statement == null) return null;
        statement.setString(1, userId);
        statement.setString(2, projectId);
        @Nullable ResultSet resultSet = statement.executeQuery();
        if (resultSet == null) return null;
        while (resultSet.next()) {
            @NotNull final Project project = new Project();
            project.setId(resultSet.getString("id"));
            project.setName(resultSet.getString("name"));
            project.setDescription(resultSet.getString("description"));
            project.setUserId(resultSet.getString("user_id"));
            project.setStatus(Status.valueOf(resultSet.getString("status")));
            project.setDateCreate(resultSet.getDate("date_create"));
            project.setDateBegin(resultSet.getDate("date_begin"));
            project.setDateEnd(resultSet.getDate("date_end"));
            return project;
        }
        return null;
    }

    public boolean merge(@NotNull final String projectName, @NotNull final String userId, @NotNull final String description) throws SQLException {
        if (findAll(userId).isEmpty()) {
            persist(new Project(projectName, RandomUtil.UUID(), userId, description));
            return true;
        }
        @Nullable final Project project = findProjectByName(projectName, userId);
        if (project == null) {
            persist(new Project(projectName, RandomUtil.UUID(), userId, description));
            return true;
        }
        update(userId, project.getId(), description, projectName);
        return true;
    }

    public void update(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final String description,
            @NotNull final String projectName) throws SQLException {
        @Nullable final Project project = findOne(id, userId);
        if (project == null) return;
        @NotNull final String query = "update project set name = ?, description = ? where user_id = ? and id = ?";
        @Nullable final PreparedStatement statement = connection.prepareStatement(query);
        if (statement == null) return;
        statement.setString(1, projectName);
        statement.setString(2, description);
        statement.setString(3, userId);
        statement.setString(4, id);
        statement.executeUpdate();
    }

    @Override
    @NotNull
    public List<Project> findProjectsByPart(@NotNull final String userId, @NotNull final String part) throws SQLException {
        @NotNull final List<Project> projects = new ArrayList<>();
        @NotNull final String query = "select * from project where user_id = ? and name like %?% or description like %?%";
        @Nullable final PreparedStatement statement = connection.prepareStatement(query);
        if (statement == null) return projects;
        statement.setString(1, userId);
        statement.setString(2, part);
        statement.setString(3, part);
        @Nullable ResultSet resultSet = statement.executeQuery();
        if (resultSet == null) return projects;
        while (resultSet.next()) {
            @NotNull final Project project = new Project();
            project.setId(resultSet.getString("id"));
            project.setName(resultSet.getString("name"));
            project.setDescription(resultSet.getString("description"));
            project.setUserId(resultSet.getString("user_id"));
            project.setStatus(Status.valueOf(resultSet.getString("status")));
            project.setDateCreate(resultSet.getDate("date_create"));
            project.setDateBegin(resultSet.getDate("date_begin"));
            project.setDateEnd(resultSet.getDate("date_end"));
            projects.add(project);
        }
        return projects;
    }


    @Override
    @NotNull
    public List<Project> findProjectsByStatus(@NotNull final String userId, @NotNull final Status status) throws SQLException {
        @NotNull final List<Project> projects = new ArrayList<>();
        @NotNull final String query = "select * from project where status= ?";
        @Nullable final PreparedStatement statement = connection.prepareStatement(query);
        if (statement == null) return projects;
        statement.setString(1, status.toString());
        @Nullable ResultSet resultSet = statement.executeQuery();
        if (resultSet == null) return projects;
        while (resultSet.next()) {
            @NotNull final Project project = new Project();
            project.setId(resultSet.getString("id"));
            project.setName(resultSet.getString("name"));
            project.setDescription(resultSet.getString("description"));
            project.setUserId(resultSet.getString("user_id"));
            project.setStatus(Status.valueOf(resultSet.getString("status")));
            project.setDateCreate(resultSet.getDate("date_create"));
            project.setDateBegin(resultSet.getDate("date_begin"));
            project.setDateEnd(resultSet.getDate("date_end"));
            projects.add(project);
        }
        return projects;
    }

    @Override
    @Nullable
    public Date startProject(@NotNull final String userId, @NotNull final String projectName) throws SQLException {
        @NotNull final Date startDate = new Date();
        @Nullable final Project project = findProjectByName(projectName, userId);
        if (project == null || !project.getStatus().equals(Status.PLANNED)) return null;
        @NotNull final String query = "update project set status = ? and set date_begin = ? where user_id = ? and name = ?";
        @Nullable final PreparedStatement statement = connection.prepareStatement(query);
        if (statement == null) return null;
        statement.setString(1, Status.PROCESS.toString());
        statement.setString(2, simpleDateFormat.format(startDate));
        statement.setString(3, userId);
        statement.setString(4, projectName);
        statement.executeUpdate();
        return startDate;
    }

    @Override
    @Nullable
    public Date endProject(@NotNull final String userId, @NotNull final String projectName) throws SQLException {
        @NotNull final Date endDate = new Date();
        @Nullable final Project project = findProjectByName(projectName, userId);
        if (project == null || !project.getStatus().equals(Status.PROCESS)) return null;
        @NotNull final String query = "update project set status = ? and set date_end = ? where user_id = ? and name = ?";
        @Nullable final PreparedStatement statement = connection.prepareStatement(query);
        if (statement == null) return null;
        statement.setString(1, Status.DONE.toString());
        statement.setString(2, simpleDateFormat.format(endDate));
        statement.setString(3, userId);
        statement.setString(4, projectName);
        statement.executeUpdate();
        return endDate;
    }

    @Override
    @NotNull
    public List<Project> findAll(@NotNull final List<User> userList) throws SQLException {
        @Nullable final Statement statement = connection.createStatement();
        @NotNull final List<Project> projects = new ArrayList<>();
        if (statement == null) return projects;
        @NotNull final String query = "select * from project ;";
        @Nullable ResultSet resultSet = statement.executeQuery(query);
        if (resultSet == null) return projects;
        while (resultSet.next()) {
            @NotNull final Project project = new Project();
            project.setId(resultSet.getString("id"));
            project.setName(resultSet.getString("name"));
            project.setDescription(resultSet.getString("description"));
            project.setUserId(resultSet.getString("user_id"));
            project.setStatus(Status.valueOf(resultSet.getString("status")));
            project.setDateCreate(resultSet.getDate("date_create"));
            project.setDateBegin(resultSet.getDate("date_begin"));
            project.setDateEnd(resultSet.getDate("date_end"));
            projects.add(project);
        }
        return projects;
    }

    @Override
    public void clear() throws SQLException {
        @Nullable final Statement statement = connection.createStatement();
        if (statement == null) return;
        @NotNull final String query = "delete from project;";
        statement.executeUpdate(query);
    }
}
