package ru.sadkov.tm.repository;

import ru.sadkov.tm.api.IRepository;
import ru.sadkov.tm.entity.AbstractEntity;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.LinkedHashMap;
import java.util.Map;

public abstract class AbstractRepository<T extends AbstractEntity> implements IRepository<T> {

    protected final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    protected Map<String, T> entities = new LinkedHashMap<>();

    @Override
    public boolean isEmpty() throws SQLException {
        return entities.isEmpty();
    }
}
