package ru.sadkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.api.IUserRepository;
import ru.sadkov.tm.entity.User;
import ru.sadkov.tm.enumeration.Role;
import ru.sadkov.tm.util.HashUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    private final Connection connection;

    public UserRepository(@NotNull Connection connection) {
        this.connection = connection;
    }

    public void persist(@NotNull final User user) throws SQLException {
        @NotNull final String query = "INSERT INTO user (id, login, password, role) VALUES(?, ?, ?, ?)";
        @Nullable final PreparedStatement statement = connection.prepareStatement(query);
        if (statement == null) return;
        @NotNull final String id = user.getId();
        @Nullable final String login = user.getLogin();
        @Nullable final String password = HashUtil.hashMD5(user.getPassword());
        @Nullable final String role = user.getRole().toString();
        statement.setString(1, id);
        statement.setString(2, login);
        statement.setString(3, password);
        statement.setString(4, role);
        statement.executeUpdate();
    }

    @Nullable
    public User findOne(@NotNull final String userId) throws SQLException {
        @NotNull final String query = "select * from user where id = ?";
        @Nullable final PreparedStatement statement = connection.prepareStatement(query);
        if (statement == null) return null;
        statement.setString(1, userId);
        @Nullable ResultSet resultSet = statement.executeQuery();
        if (resultSet == null) return null;
        @NotNull final User user = new User();
        while (resultSet.next()) {
            user.setId(resultSet.getString("id"));
            user.setLogin(resultSet.getString("login"));
            user.setPassword(resultSet.getString("password"));
            user.setRole(Role.valueOf(resultSet.getString("role")));
            return user;
        }
        return null;
    }

    @Nullable
    public User findByLogin(@NotNull final String login) throws SQLException {
        @NotNull final String query = "SELECT * FROM user WHERE login = ? ";
        @Nullable final PreparedStatement statement = connection.prepareStatement(query);
        if (statement == null) return null;
        statement.setString(1, login);
        @Nullable final ResultSet resultSet = statement.executeQuery();
        if (resultSet == null) return null;
        @NotNull final User user = new User();
        while (resultSet.next()) {
            user.setId(resultSet.getString("id"));
            user.setLogin(resultSet.getString("login"));
            user.setPassword(resultSet.getString("password"));
            user.setRole(Role.valueOf(resultSet.getString("role")));
            return user;
        }
        return null;
    }

    @NotNull
    public List<User> findAll() throws SQLException {
        @NotNull final List<User> users = new ArrayList<>();
        @Nullable final Statement statement = connection.createStatement();
        if (statement == null) return users;
        @NotNull final String query = "select * from user;";
        @Nullable ResultSet resultSet = statement.executeQuery(query);
        if (resultSet == null) return users;
        while (resultSet.next()) {
            @NotNull final User user = new User();
            user.setId(resultSet.getString("id"));
            user.setLogin(resultSet.getString("login"));
            user.setPassword(resultSet.getString("password"));
            user.setRole(Role.valueOf(resultSet.getString("role")));
            users.add(user);
        }
        return users;
    }

    public void removeByLogin(@NotNull final String login) throws SQLException {
        @NotNull final String query = "delete from user where login = ?";
        @Nullable final PreparedStatement statement = connection.prepareStatement(query);
        if (statement == null) return;
        statement.setString(1, login);
        statement.executeUpdate();
    }

    public void removeAll() throws SQLException {
        @Nullable final Statement statement = connection.createStatement();
        if (statement == null) return;
        @NotNull final String query = "delete from user;";
        statement.executeUpdate(query);
    }

    public void update(@NotNull final String userId, @NotNull final String login) throws SQLException {
        @NotNull final String query = "update user set login = ? where id = ?";
        @Nullable final PreparedStatement statement = connection.prepareStatement(query);
        if (statement == null) return;
        statement.setString(1, login);
        statement.setString(2, userId);
        statement.executeUpdate(query);

    }

    @Override
    public void clear() throws SQLException {
        @Nullable final Statement statement = connection.createStatement();
        if (statement == null) return;
        @NotNull final String query = "delete from user;";
        statement.executeUpdate(query);
    }
}
