package ru.sadkov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.entity.Project;
import ru.sadkov.tm.entity.User;
import ru.sadkov.tm.enumeration.Status;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;
import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    @NotNull
    Collection<Project> findAll(@NotNull final String userId) throws SQLException;

    void remove(@NotNull final String userId, @NotNull final String projectName) throws SQLException;

    @Nullable Project findProjectByName(@NotNull String name, @NotNull String userId) throws SQLException;

    void removeAll(@NotNull final String userId) throws SQLException;

    @Nullable
    Project findOne(@NotNull final String projectId, @NotNull final String userId) throws SQLException;

    boolean merge(@NotNull final String projectName, @NotNull final String userId, @NotNull final String description) throws SQLException;

    void update(@NotNull final String userId, @NotNull final String id, @NotNull final String description, @NotNull final String projectName) throws SQLException;

    @NotNull
    List<Project> findProjectsByPart(@NotNull final String userId, @NotNull final String part) throws SQLException;

    @Nullable
    Date startProject(@NotNull final String userId, @NotNull final String projectName) throws SQLException;

    @Nullable
    Date endProject(@NotNull final String userId, @NotNull final String projectName) throws SQLException;

    @NotNull
    List<Project> findProjectsByStatus(@NotNull final String userId, @NotNull final Status status) throws SQLException;

    @NotNull
    List<Project> findAll(@NotNull final List<User> userList) throws SQLException;
}
