package ru.sadkov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.entity.User;

import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.List;

public interface IUserService {

    void userRegister(@Nullable final User user) throws NoSuchAlgorithmException;

    boolean login(@Nullable final String login, @Nullable final String password);

    @Nullable
    User findOneByLogin(@Nullable final String login);


    void addTestUsers() throws NoSuchAlgorithmException;

    @NotNull
    List<User> findAll();

    void clear();

    void userAddFromData(@Nullable final User user);

    void load(@Nullable final List<User> users);

    @Nullable
    User findOneById(@NotNull final String userId);
}
