package ru.sadkov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.entity.Task;
import ru.sadkov.tm.entity.User;
import ru.sadkov.tm.enumeration.Status;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    @NotNull
    List<Task> findAll(@NotNull final String userId) throws SQLException;

    void removeAll(@NotNull final String userId) throws SQLException;

    @Nullable
    Task findOne(@NotNull final String taskId, @NotNull final String userId) throws SQLException;

    void merge(@NotNull final String taskName, @NotNull final String projectId, @NotNull final String userId) throws SQLException;

    void update(@NotNull final String taskId, @NotNull final String taskName, @NotNull final String userId) throws SQLException;

    @Nullable
    Task findTaskByName(@NotNull final String taskName, @NotNull final String userId) throws SQLException;

    void removeByName(@NotNull final String taskName, @NotNull final String userId) throws SQLException;

    @NotNull
    List<Task> getTasksByPart(@NotNull final String userId, @NotNull final String part) throws SQLException;

    @NotNull
    List<Task> findTasksByStatus(@NotNull final String userId, @NotNull final Status status) throws SQLException;

    @Nullable
    Date startTask(@NotNull final String userId, @NotNull final String taskName) throws SQLException;

    @Nullable
    Date endTask(@NotNull final String userId, @NotNull final String taskName) throws SQLException;

    @NotNull
    List<Task> findAll(@NotNull final List<User> userList) throws SQLException;
}
