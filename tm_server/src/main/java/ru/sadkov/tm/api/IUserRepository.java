package ru.sadkov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.entity.User;

import java.sql.SQLException;
import java.util.List;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    User findOne(@NotNull final String userId) throws SQLException;

    @Nullable
    User findByLogin(@NotNull final String login) throws SQLException;

    @NotNull
    List<User> findAll() throws SQLException;

    void removeAll() throws SQLException;

    void update(@NotNull final String userId, @NotNull final String login) throws SQLException;

    void removeByLogin(@NotNull final String login) throws SQLException;
}
