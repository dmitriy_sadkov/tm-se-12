package ru.sadkov.tm.api;

import org.jetbrains.annotations.NotNull;

import java.sql.SQLException;

public interface IRepository<T> {

    void persist(@NotNull T t) throws SQLException;

    boolean isEmpty() throws SQLException;

    void clear() throws SQLException;
}
