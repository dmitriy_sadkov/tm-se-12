package ru.sadkov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.service.PropertyService;

import java.io.InputStream;

public interface IPropertyService {

    void init() throws Exception;

    @NotNull
    String getServerHost();

    @NotNull
    String getServerPort();

    @NotNull
    String getSessionSalt();

    Integer getSessionCycle();
}
