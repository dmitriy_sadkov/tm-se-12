package ru.sadkov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.entity.Session;

import java.sql.SQLException;
import java.util.List;

public interface ISessionRepository extends IRepository<Session> {

    @NotNull
    List<Session> findByUserId(@NotNull final String userId) throws SQLException;

    boolean contains(final Session id) throws SQLException;

    void close(@NotNull final Session session) throws SQLException;

    List<Session> findAll() throws SQLException;
}
