package ru.sadkov.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.sadkov.tm.service.ComparatorService;

import java.util.Scanner;


public interface ServiceLocator {

    @NotNull
    IUserService getUserService();

    @NotNull
    IDomainService getDomainService();

    @NotNull
    Scanner getScanner();

    @NotNull
    IProjectService getProjectService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    ComparatorService getComparatorService();

    @NotNull
    ISessionService getSessionService();

    @NotNull
    IPropertyService getPropertyService();
}
