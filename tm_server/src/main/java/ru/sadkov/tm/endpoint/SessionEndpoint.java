package ru.sadkov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.api.ServiceLocator;
import ru.sadkov.tm.entity.Session;
import ru.sadkov.tm.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public final class SessionEndpoint {

    @NotNull
    final ServiceLocator serviceLocator;

    public SessionEndpoint(@NotNull ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @WebMethod
    public Session openSession(
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "password") @Nullable final String password
    ) {
        return serviceLocator.getSessionService().open(login, password);
    }

    @WebMethod
    public boolean isValid(
            @WebParam(name = "session")@Nullable final Session session){
        return serviceLocator.getSessionService().isValid(session);
    }

    @WebMethod
    public boolean closeSession(
            @WebParam(name = "session")@Nullable final Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        try{
            serviceLocator.getSessionService().close(session);
            return true;
        }catch (final java.lang.Exception e){
            return false;
        }
    }

    @WebMethod
    public boolean closeSessionAll(
            @WebParam(name = "session")@Nullable final Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        try{
            serviceLocator.getSessionService().closeAll(session);
            return true;
        }catch (final java.lang.Exception e){
            return false;
        }
    }

    @WebMethod
    public List<Session>getListSession(
            @WebParam(name = "session")@Nullable final Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getSessionService().getListSession(session);
    }

    @WebMethod
    public User getUser(
            @WebParam(name = "session")@Nullable final Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getSessionService().getUser(session);
    }
}
