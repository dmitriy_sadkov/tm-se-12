package ru.sadkov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.api.ServiceLocator;
import ru.sadkov.tm.entity.AbstractEntity;
import ru.sadkov.tm.entity.Project;
import ru.sadkov.tm.entity.Session;
import ru.sadkov.tm.entity.User;
import ru.sadkov.tm.enumeration.Role;
import ru.sadkov.tm.enumeration.Status;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Comparator;
import java.util.List;

@WebService
public final class ProjectEndpoint {

    @NotNull
    private final ServiceLocator serviceLocator;

    public ProjectEndpoint(@NotNull ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @WebMethod
    @Nullable
    public List<Project> getSortedProjectList(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "comparator") @Nullable final String comparatorName) throws Exception {
        serviceLocator.getSessionService().validate(session);
        Comparator<? extends AbstractEntity> comparator = serviceLocator.getComparatorService().getComparator(comparatorName);
        return serviceLocator.getProjectService().getSortedProjectList(session.getUserId(), comparator);
    }

    @WebMethod
    @Nullable
    public String findProjectIdByName(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "projectName") @Nullable final String projectName) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findProjectIdByName(projectName, session.getUserId());
    }

    @WebMethod
    public boolean createProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "projectName") @Nullable final String projectName,
            @WebParam(name = "description") @Nullable final String description) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().persist(projectName, session.getUserId(), description);
    }

    @WebMethod
    public void removeProjectByName(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "projectName") @Nullable final String projectName) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().removeByName(projectName, session.getUserId());
    }

    @Nullable
    @WebMethod
    public List<Project> findAllProjectsForUser(
            @WebParam(name = "session") @Nullable final Session session) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findAll(serviceLocator.getSessionService().getUser(session));
    }

    @WebMethod
    public void removeAllProjectsForUser(
            @WebParam(name = "session") @Nullable final Session session) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().removeAll(serviceLocator.getSessionService().getUser(session));
    }

    @WebMethod
    public void updateProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "project") @Nullable final Project project,
            @WebParam(name = "projectName") @Nullable final String projectName,
            @WebParam(name = "description") @Nullable final String description) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().update(project, projectName, description);
    }

    @Nullable
    @WebMethod
    public Project findOneProjectByName(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "projectName") @Nullable final String projectName) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findOneByName(projectName, serviceLocator.getSessionService().getUser(session));
    }


    @Nullable
    @WebMethod
    public List<Project> findProjectsByPart(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "part") @Nullable final String part) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findProjectsByPart(session.getUserId(), part);
    }

    @Nullable
    @WebMethod
    public List<Project> findProjectsByStatus(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "status") @Nullable final Status status) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findProjectsByStatus(session.getUserId(), status);
    }


    @Nullable
    @WebMethod
    public String startProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "projectName") @Nullable final String projectName) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().startProject(session.getUserId(), projectName);
    }

    @WebMethod
    public @Nullable String endProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "projectName") @Nullable final String projectName) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().endProject(session.getUserId(), projectName);
    }

    @NotNull
    @WebMethod
    public List<Project> findAllProjects(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "userList") @NotNull final List<User> userList) throws Exception {
        serviceLocator.getSessionService().validate(session,Role.ADMIN);
        return serviceLocator.getProjectService().findAll(userList);
    }

    @WebMethod
    public void persistProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "project") @Nullable final Project project) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().persist(project);
    }

    @WebMethod
    public void clearProjects(
            @WebParam(name = "session") @Nullable final Session session) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getProjectService().clear();
    }

    @WebMethod
    public void loadProjects(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "projectList") @Nullable final List<Project> projects) throws Exception {
        serviceLocator.getSessionService().validate(session,Role.ADMIN);
        serviceLocator.getProjectService().load(projects);
    }
}
