package ru.sadkov.tm.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.api.ServiceLocator;
import ru.sadkov.tm.entity.Session;
import ru.sadkov.tm.enumeration.Role;
import ru.sadkov.tm.exception.WrongDataException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public final class DomainEndpoint {

    private final ServiceLocator serviceLocator;

    public DomainEndpoint(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @WebMethod
    public boolean domainSaveJacksonJSON(
            @WebParam(name = "session") @Nullable final Session session)  {
        try {
            serviceLocator.getSessionService().validate(session, Role.ADMIN);
            serviceLocator.getDomainService().domainSaveJacksonJSON();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @WebMethod
    public boolean domainSaveJacksonXML(
            @WebParam(name = "session") @Nullable final Session session) throws java.lang.Exception {
        try {
            serviceLocator.getSessionService().validate(session, Role.ADMIN);
            serviceLocator.getDomainService().domainSaveJacksonXML();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @WebMethod
    public boolean domainSaveXML(
            @WebParam(name = "session") @Nullable final Session session) throws java.lang.Exception {
        try {
            serviceLocator.getSessionService().validate(session, Role.ADMIN);
            serviceLocator.getDomainService().domainSaveXML();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @WebMethod
    public boolean domainSaveJSON(
            @WebParam(name = "session") @Nullable final Session session) throws java.lang.Exception {
        try {
            serviceLocator.getSessionService().validate(session, Role.ADMIN);
            serviceLocator.getDomainService().domainSaveJSON();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @WebMethod
    public boolean domainSerialize(@WebParam(name = "session") @Nullable final Session session) throws java.lang.Exception {
        try {
            serviceLocator.getSessionService().validate(session, Role.ADMIN);
            serviceLocator.getDomainService().domainSerialize();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @WebMethod
    public boolean domainDeserialize(@WebParam(name = "session") @Nullable final Session session) throws java.lang.Exception {
        try {
            serviceLocator.getSessionService().validate(session, Role.ADMIN);
            serviceLocator.getDomainService().domainDeserialize();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @WebMethod
    public boolean domainLoadJSON(
            @WebParam(name = "session") @Nullable final Session session) throws java.lang.Exception {
        try {
            serviceLocator.getSessionService().validate(session, Role.ADMIN);
            serviceLocator.getDomainService().domainLoadJSON();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @WebMethod
    public boolean domainLoadXML(
            @WebParam(name = "session") @Nullable final Session session) throws java.lang.Exception {
        try {
            serviceLocator.getSessionService().validate(session, Role.ADMIN);
            serviceLocator.getDomainService().domainLoadXML();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @WebMethod
    public boolean domainLoadJacksonJSON(
            @WebParam(name = "session") @Nullable final Session session) throws java.lang.Exception {
        try {
            serviceLocator.getSessionService().validate(session, Role.ADMIN);
            serviceLocator.getDomainService().domainLoadJacksonJSON();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @WebMethod
    public boolean domainLoadJacksonXML(
            @WebParam(name = "session") @Nullable final Session session) throws java.lang.Exception {
        try {
            serviceLocator.getSessionService().validate(session, Role.ADMIN);
            serviceLocator.getDomainService().domainLoadJacksonXML();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
