package ru.sadkov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.api.ServiceLocator;
import ru.sadkov.tm.entity.AbstractEntity;
import ru.sadkov.tm.entity.Session;
import ru.sadkov.tm.entity.Task;
import ru.sadkov.tm.entity.User;
import ru.sadkov.tm.enumeration.Role;
import ru.sadkov.tm.enumeration.Status;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Comparator;
import java.util.List;

@WebService
public final class TaskEndpoint {

    private final ServiceLocator serviceLocator;

    public TaskEndpoint(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }


    @Nullable
    @WebMethod
    public Task findTaskByName(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "taskName") @Nullable final String taskName) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findTaskByName(taskName, session.getUserId());
    }

    @Nullable
    @WebMethod
    public List<Task> getSortedTaskList(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "comparator") @Nullable final String comparatorName) throws Exception {
        serviceLocator.getSessionService().validate(session);
        Comparator<? extends AbstractEntity> comparator = serviceLocator.getComparatorService().getComparator(comparatorName);
        return serviceLocator.getTaskService().getSortedTaskList(session.getUserId(), comparator);
    }

    @WebMethod
    public boolean saveTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "taskName") @Nullable final String taskName,
            @WebParam(name = "projectName") @Nullable final String projectName) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().saveTask(taskName, projectName, serviceLocator.getSessionService().getUser(session));
    }

    @WebMethod
    public void removeTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "taskName") @Nullable final String taskName) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().removeTask(taskName, serviceLocator.getSessionService().getUser(session));
    }

    @Nullable
    @WebMethod
    public List<Task> findAllTasksForUser(
            @WebParam(name = "session") @Nullable final Session session) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findAll(serviceLocator.getSessionService().getUser(session));
    }

    @WebMethod
    public void removeAllTasksForUser(
            @WebParam(name = "session") @Nullable final Session session) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().removeAll(serviceLocator.getSessionService().getUser(session));
    }

    @WebMethod
    public void removeTaskForProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "projectName") @Nullable final String projectName) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().removeTaskForProject(projectName, serviceLocator.getSessionService().getUser(session));
    }

    @WebMethod
    public void updateTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "oldName") @Nullable final String oldName,
            @WebParam(name = "newName") @Nullable final String newName) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().update(oldName, newName, session.getUserId());
    }

    @Nullable
    @WebMethod
    public List<Task> getTasksByPart(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "part") @Nullable final String part) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().getTasksByPart(session.getUserId(), part);
    }

    @Nullable
    @WebMethod
    public List<Task> findTasksByStatus(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "status") @Nullable final Status status) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findTasksByStatus(session.getUserId(), status);
    }


    @Nullable
    @WebMethod
    public String startTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "taskName") @Nullable final String taskName) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().startTask(session.getUserId(), taskName);
    }

    @Nullable
    @WebMethod
    public String endTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "taskName") @Nullable final String taskName) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().endTask(session.getUserId(), taskName);
    }


    @NotNull
    @WebMethod
    public List<Task> findAllTasks(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "userList") @NotNull final List<User> userList) throws Exception {
        serviceLocator.getSessionService().validate(session,Role.ADMIN);
        return serviceLocator.getTaskService().findAll(userList);
    }


    @WebMethod
    public void persistTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "task") @Nullable final Task task) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().persist(task);
    }


    @WebMethod
    public void clearTasks(
            @WebParam(name = "session") @Nullable final Session session) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getTaskService().clear();
    }


    @WebMethod
    public void loadTasks(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "taskList") @Nullable final List<Task> tasks) throws Exception {
        serviceLocator.getSessionService().validate(session,Role.ADMIN);
        serviceLocator.getTaskService().load(tasks);
    }
}
