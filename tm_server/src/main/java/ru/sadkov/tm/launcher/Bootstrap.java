package ru.sadkov.tm.launcher;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.api.*;
import ru.sadkov.tm.endpoint.*;
import ru.sadkov.tm.service.*;


import javax.xml.ws.Endpoint;
import java.security.NoSuchAlgorithmException;
import java.util.*;

public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final Scanner scanner = new Scanner(System.in);
    @NotNull
    private final IProjectService projectService = new ProjectService();
    @NotNull
    private final ITaskService taskService = new TaskService(projectService);
    @NotNull
    private final IUserService userService = new UserService();
    @NotNull
    private final IDomainService domainService = new DomainService(this);
    @NotNull
    private final UserEndpoint userEndpoint = new UserEndpoint(this);
    @NotNull
    private final ProjectEndpoint projectEndpoint = new ProjectEndpoint(this);
    @NotNull
    private final TaskEndpoint taskEndpoint = new TaskEndpoint(this);
    @NotNull
    private final DomainEndpoint domainEndpoint = new DomainEndpoint(this);
    @NotNull
    private final ComparatorService comparatorService = new ComparatorService();
    @NotNull
    private final ISessionService sessionService = new SessionService(this);
    @NotNull
    private final SessionEndpoint sessionEndpoint = new SessionEndpoint(this);
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Override
    public @NotNull IPropertyService getPropertyService() {
        return propertyService;
    }

    @Override
    public @NotNull ISessionService getSessionService() {
        return sessionService;
    }

    @NotNull
    public ComparatorService getComparatorService() {
        return comparatorService;
    }

    @NotNull
    public IUserService getUserService() {
        return userService;
    }

    @NotNull
    public Scanner getScanner() {
        return scanner;
    }

    @NotNull
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public @NotNull IDomainService getDomainService() {
        return domainService;
    }


    public void init() {
        initUsers();
        initProperty();
        initEndpoint();
    }

    private void initProperty() {
        try {
            propertyService.init();
        } catch (Exception e) {
            System.out.println("Properties error");
        }
    }

    private void initEndpoint() {
        registry(userEndpoint);
        registry(domainEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);
        registry(sessionEndpoint);
    }

    private void registry(@Nullable final Object endpoint) {
        if (endpoint == null) return;
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + name + "?wsdl";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

    private void initUsers() {
        try {
            userService.addTestUsers();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }
}
