package ru.sadkov.tm.comparator;

import org.jetbrains.annotations.NotNull;
import ru.sadkov.tm.entity.Project;

import java.util.Comparator;

public final class ProjectStatusComparator implements Comparator<Project> {

    @Override
    public int compare(@NotNull final Project project1, @NotNull final Project project2) {
        final int result = project1.getStatus().compareTo(project2.getStatus());
        if (result == 0) return project1.getId().compareTo(project2.getId());
        return result;
    }
}
