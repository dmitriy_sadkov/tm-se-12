package ru.sadkov.tm.comparator;

import org.jetbrains.annotations.NotNull;
import ru.sadkov.tm.entity.Project;

import java.util.Comparator;

public final class ProjectDateStartComparator implements Comparator<Project> {

    @Override
    public int compare(@NotNull final Project project1, @NotNull final Project project2) {
        if (project1.getDateBegin() == null) return Integer.MIN_VALUE;
        if (project2.getDateBegin() == null) return Integer.MAX_VALUE;
        final int result = project1.getDateBegin().compareTo(project2.getDateBegin());
        if (result == 0) return project1.getId().compareTo(project2.getId());
        return result;
    }
}
