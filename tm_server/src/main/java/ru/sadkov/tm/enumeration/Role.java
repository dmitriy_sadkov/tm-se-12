package ru.sadkov.tm.enumeration;

import org.jetbrains.annotations.NotNull;

public enum Role {

    USER("user"),
    ADMIN("admin");

    @NotNull
    private String displayName;

    Role(@NotNull final String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }
}
