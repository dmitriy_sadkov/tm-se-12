package ru.sadkov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.endpoint.*;

import java.util.Map;
import java.util.Scanner;

public interface ServiceLocator {
    @NotNull
    Scanner getScanner();

    @NotNull
    DomainEndpoint getDomainEndpoint();

    @NotNull
    ProjectEndpoint getProjectEndpoint();

    @NotNull
    TaskEndpoint getTaskEndpoint();

    @NotNull
    UserEndpoint getUserEndpoint();

    @NotNull
    Map<String, AbstractCommand> getCommandMap();

    @Nullable
    Session getCurrentSession();

    void setCurrentSession(@Nullable final Session session);

    @NotNull
    SessionEndpoint getSessionEndpoint();
}
