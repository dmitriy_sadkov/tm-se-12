
package ru.sadkov.tm.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.sadkov.tm.endpoint package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Exception_QNAME = new QName("http://endpoint.tm.sadkov.ru/", "Exception");
    private final static QName _CloseSession_QNAME = new QName("http://endpoint.tm.sadkov.ru/", "closeSession");
    private final static QName _CloseSessionAll_QNAME = new QName("http://endpoint.tm.sadkov.ru/", "closeSessionAll");
    private final static QName _CloseSessionAllResponse_QNAME = new QName("http://endpoint.tm.sadkov.ru/", "closeSessionAllResponse");
    private final static QName _CloseSessionResponse_QNAME = new QName("http://endpoint.tm.sadkov.ru/", "closeSessionResponse");
    private final static QName _GetListSession_QNAME = new QName("http://endpoint.tm.sadkov.ru/", "getListSession");
    private final static QName _GetListSessionResponse_QNAME = new QName("http://endpoint.tm.sadkov.ru/", "getListSessionResponse");
    private final static QName _GetUser_QNAME = new QName("http://endpoint.tm.sadkov.ru/", "getUser");
    private final static QName _GetUserResponse_QNAME = new QName("http://endpoint.tm.sadkov.ru/", "getUserResponse");
    private final static QName _IsValid_QNAME = new QName("http://endpoint.tm.sadkov.ru/", "isValid");
    private final static QName _IsValidResponse_QNAME = new QName("http://endpoint.tm.sadkov.ru/", "isValidResponse");
    private final static QName _OpenSession_QNAME = new QName("http://endpoint.tm.sadkov.ru/", "openSession");
    private final static QName _OpenSessionResponse_QNAME = new QName("http://endpoint.tm.sadkov.ru/", "openSessionResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.sadkov.tm.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Exception }
     * 
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link CloseSession }
     * 
     */
    public CloseSession createCloseSession() {
        return new CloseSession();
    }

    /**
     * Create an instance of {@link CloseSessionAll }
     * 
     */
    public CloseSessionAll createCloseSessionAll() {
        return new CloseSessionAll();
    }

    /**
     * Create an instance of {@link CloseSessionAllResponse }
     * 
     */
    public CloseSessionAllResponse createCloseSessionAllResponse() {
        return new CloseSessionAllResponse();
    }

    /**
     * Create an instance of {@link CloseSessionResponse }
     * 
     */
    public CloseSessionResponse createCloseSessionResponse() {
        return new CloseSessionResponse();
    }

    /**
     * Create an instance of {@link GetListSession }
     * 
     */
    public GetListSession createGetListSession() {
        return new GetListSession();
    }

    /**
     * Create an instance of {@link GetListSessionResponse }
     * 
     */
    public GetListSessionResponse createGetListSessionResponse() {
        return new GetListSessionResponse();
    }

    /**
     * Create an instance of {@link GetUser }
     * 
     */
    public GetUser createGetUser() {
        return new GetUser();
    }

    /**
     * Create an instance of {@link GetUserResponse }
     * 
     */
    public GetUserResponse createGetUserResponse() {
        return new GetUserResponse();
    }

    /**
     * Create an instance of {@link IsValid }
     * 
     */
    public IsValid createIsValid() {
        return new IsValid();
    }

    /**
     * Create an instance of {@link IsValidResponse }
     * 
     */
    public IsValidResponse createIsValidResponse() {
        return new IsValidResponse();
    }

    /**
     * Create an instance of {@link OpenSession }
     * 
     */
    public OpenSession createOpenSession() {
        return new OpenSession();
    }

    /**
     * Create an instance of {@link OpenSessionResponse }
     * 
     */
    public OpenSessionResponse createOpenSessionResponse() {
        return new OpenSessionResponse();
    }

    /**
     * Create an instance of {@link Session }
     * 
     */
    public Session createSession() {
        return new Session();
    }

    /**
     * Create an instance of {@link User }
     * 
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.sadkov.ru/", name = "Exception")
    public JAXBElement<Exception> createException(Exception value) {
        return new JAXBElement<Exception>(_Exception_QNAME, Exception.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CloseSession }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.sadkov.ru/", name = "closeSession")
    public JAXBElement<CloseSession> createCloseSession(CloseSession value) {
        return new JAXBElement<CloseSession>(_CloseSession_QNAME, CloseSession.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CloseSessionAll }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.sadkov.ru/", name = "closeSessionAll")
    public JAXBElement<CloseSessionAll> createCloseSessionAll(CloseSessionAll value) {
        return new JAXBElement<CloseSessionAll>(_CloseSessionAll_QNAME, CloseSessionAll.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CloseSessionAllResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.sadkov.ru/", name = "closeSessionAllResponse")
    public JAXBElement<CloseSessionAllResponse> createCloseSessionAllResponse(CloseSessionAllResponse value) {
        return new JAXBElement<CloseSessionAllResponse>(_CloseSessionAllResponse_QNAME, CloseSessionAllResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CloseSessionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.sadkov.ru/", name = "closeSessionResponse")
    public JAXBElement<CloseSessionResponse> createCloseSessionResponse(CloseSessionResponse value) {
        return new JAXBElement<CloseSessionResponse>(_CloseSessionResponse_QNAME, CloseSessionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetListSession }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.sadkov.ru/", name = "getListSession")
    public JAXBElement<GetListSession> createGetListSession(GetListSession value) {
        return new JAXBElement<GetListSession>(_GetListSession_QNAME, GetListSession.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetListSessionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.sadkov.ru/", name = "getListSessionResponse")
    public JAXBElement<GetListSessionResponse> createGetListSessionResponse(GetListSessionResponse value) {
        return new JAXBElement<GetListSessionResponse>(_GetListSessionResponse_QNAME, GetListSessionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.sadkov.ru/", name = "getUser")
    public JAXBElement<GetUser> createGetUser(GetUser value) {
        return new JAXBElement<GetUser>(_GetUser_QNAME, GetUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.sadkov.ru/", name = "getUserResponse")
    public JAXBElement<GetUserResponse> createGetUserResponse(GetUserResponse value) {
        return new JAXBElement<GetUserResponse>(_GetUserResponse_QNAME, GetUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsValid }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.sadkov.ru/", name = "isValid")
    public JAXBElement<IsValid> createIsValid(IsValid value) {
        return new JAXBElement<IsValid>(_IsValid_QNAME, IsValid.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsValidResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.sadkov.ru/", name = "isValidResponse")
    public JAXBElement<IsValidResponse> createIsValidResponse(IsValidResponse value) {
        return new JAXBElement<IsValidResponse>(_IsValidResponse_QNAME, IsValidResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpenSession }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.sadkov.ru/", name = "openSession")
    public JAXBElement<OpenSession> createOpenSession(OpenSession value) {
        return new JAXBElement<OpenSession>(_OpenSession_QNAME, OpenSession.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpenSessionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.sadkov.ru/", name = "openSessionResponse")
    public JAXBElement<OpenSessionResponse> createOpenSessionResponse(OpenSessionResponse value) {
        return new JAXBElement<OpenSessionResponse>(_OpenSessionResponse_QNAME, OpenSessionResponse.class, null, value);
    }

}
