package ru.sadkov.tm.command.project;

import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.endpoint.Exception_Exception;

public final class ProjectRemoveAllCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project-clear";
    }

    @Override
    public String description() {
        return "Remove all project";
    }

    @Override
    public void execute() throws Exception_Exception {
        System.out.println("[PROJECTS CLEAR]");
        serviceLocator.getTaskEndpoint().removeAllTasksForUser(serviceLocator.getCurrentSession());
        serviceLocator.getProjectEndpoint().removeAllProjectsForUser(serviceLocator.getCurrentSession());
    }

    @Override
    public boolean safe() {
        return false;
    }
}
