package ru.sadkov.tm.command.project;


import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.exception.WrongDataException;

public final class ProjectCreateCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project-create";
    }

    @Override
    public String description() {
        return "Create new project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT CREATE]");
        System.out.println("[ENTER NAME:]");
        @Nullable final String projectName = serviceLocator.getScanner().nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        @Nullable final String description = serviceLocator.getScanner().nextLine();
        if (serviceLocator.getCurrentSession() == null) {
            System.out.println("[PLEASE LOGIN]");
            return;
        }
        if (serviceLocator.getProjectEndpoint().createProject(serviceLocator.getCurrentSession(),projectName, description)) {
            System.out.println("[OK]");
            return;
        }
        throw new WrongDataException();
    }

    @Override
    public boolean safe() {
        return false;
    }
}
