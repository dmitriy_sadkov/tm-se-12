package ru.sadkov.tm.command.task;

import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.endpoint.Exception_Exception;

public final class TaskRemoveAllCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task-clear";
    }

    @Override
    public String description() {
        return "Remove all tasks";
    }

    @Override
    public void execute() throws Exception_Exception {
        System.out.println("[TASKS CLEAR]");
        serviceLocator.getTaskEndpoint().removeAllTasksForUser(serviceLocator.getCurrentSession());
        System.out.println("[OK]");
    }

    @Override
    public boolean safe() {
        return false;
    }
}
