package ru.sadkov.tm.command.project;

import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.endpoint.Project;
import ru.sadkov.tm.exception.WrongDataException;
import ru.sadkov.tm.util.ListShowUtil;

import java.util.List;

public final class ProjectSortByDateBeginCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project-sort-by-beginDate";
    }

    @Override
    public String description() {
        return "show projects sorted by begin date";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECTS SORTED BY BEGIN DATE]");
        if (serviceLocator.getCurrentSession() == null) throw new WrongDataException();
        @Nullable final List<Project> projects = serviceLocator.getProjectEndpoint()
                .getSortedProjectList(serviceLocator.getCurrentSession(),"projectDateStart");
        ListShowUtil.showList(projects);
    }

    @Override
    public boolean safe() {
        return false;
    }
}
