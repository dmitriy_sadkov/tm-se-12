package ru.sadkov.tm.command.data;

import ru.sadkov.tm.command.AbstractCommand;

public class DomainSerializeCommand extends AbstractCommand {

    @Override
    public String command() {
        return "serialize";
    }

    @Override
    public String description() {
        return "Serialize domen";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SAVING TO FILE]");
        if (serviceLocator.getDomainEndpoint().domainSerialize(serviceLocator.getCurrentSession())) return;
        System.out.println("ACCESS FORBIDDEN");
    }

    @Override
    public boolean safe() {
        return true;
    }
}
