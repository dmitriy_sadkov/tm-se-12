package ru.sadkov.tm.command.task;

import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.endpoint.Task;
import ru.sadkov.tm.exception.WrongDataException;
import ru.sadkov.tm.util.ListShowUtil;

import java.util.List;

public final class TaskSortByStatusCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task-sort-by-status";
    }

    @Override
    public String description() {
        return "show tasks sorted by status";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASKS SORTED BY STATUS]");
        if (serviceLocator.getCurrentSession() == null) throw new WrongDataException();
        @Nullable final List<Task> tasks = serviceLocator.getTaskEndpoint()
                .getSortedTaskList(serviceLocator.getCurrentSession(), "taskStatus");
        ListShowUtil.showList(tasks);
    }

    @Override
    public boolean safe() {
        return false;
    }
}
