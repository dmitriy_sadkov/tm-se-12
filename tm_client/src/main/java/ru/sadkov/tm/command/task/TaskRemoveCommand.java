package ru.sadkov.tm.command.task;

import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.endpoint.Exception_Exception;
import ru.sadkov.tm.exception.WrongDataException;

public final class TaskRemoveCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task-removeByName";
    }

    @Override
    public String description() {
        return "Remove task";
    }

    @Override
    public void execute() throws WrongDataException, Exception_Exception {
        System.out.println("[TASK REMOVE]");
        System.out.println("[ENTER NAME:]");
        @Nullable final String taskName = serviceLocator.getScanner().nextLine();
        if (taskName == null || taskName.isEmpty() || serviceLocator.getCurrentSession() == null)
            throw new WrongDataException();
        serviceLocator.getTaskEndpoint().removeTask(serviceLocator.getCurrentSession(),taskName);
        System.out.println("[OK]");
    }

    @Override
    public boolean safe() {
        return false;
    }
}
