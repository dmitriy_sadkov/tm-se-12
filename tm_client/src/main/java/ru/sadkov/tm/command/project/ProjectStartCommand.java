package ru.sadkov.tm.command.project;


import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.endpoint.Project;
import ru.sadkov.tm.endpoint.Status;
import ru.sadkov.tm.exception.WrongDataException;
import ru.sadkov.tm.util.ListShowUtil;

import java.util.List;

public final class ProjectStartCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project-start";
    }

    @Override
    public String description() {
        return "Start project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[START PROJECT]");
        System.out.println("[CHOOSE PROJECT TO START]");
        if (serviceLocator.getCurrentSession() == null) throw new WrongDataException("NO USER");
        @Nullable final List<Project> projects = serviceLocator.getProjectEndpoint()
                .findProjectsByStatus(serviceLocator.getCurrentSession(), Status.PLANNED);
        if (projects == null || projects.isEmpty()) throw new WrongDataException("[NO PLANNED PROJECT]");
        ListShowUtil.showList(projects);
        @Nullable final String projectName = serviceLocator.getScanner().nextLine();
        if (projectName == null || projectName.isEmpty()) throw new WrongDataException("[WRONG PROJECT NAME]");
        @Nullable final String startDate = serviceLocator.getProjectEndpoint()
                .startProject(serviceLocator.getCurrentSession(), projectName);
        if (startDate == null || startDate.isEmpty())
            throw new WrongDataException("[ERROR! CANT'T START PROJECT]");
        System.out.println("[PROJECT: " + projectName.toUpperCase() + " STARTS]");
        System.out.println(("[START DATE: " + startDate + " ]"));
    }

    @Override
    public boolean safe() {
        return false;
    }
}
