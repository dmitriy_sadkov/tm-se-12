package ru.sadkov.tm.command.data;

import ru.sadkov.tm.command.AbstractCommand;

public final class DomainLoadFromXMLCommand extends AbstractCommand {

    @Override
    public String command() {
        return "load-XML";
    }

    @Override
    public String description() {
        return "Load domain from in XML";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[READING FROM XML]");
        if(serviceLocator.getDomainEndpoint().domainLoadXML(serviceLocator.getCurrentSession())) return;
        System.out.println("ACCESS FORBIDDEN");
    }

    @Override
    public boolean safe() {
        return true;
    }
}
