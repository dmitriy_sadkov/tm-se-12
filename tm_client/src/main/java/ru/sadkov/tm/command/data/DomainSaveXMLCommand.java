package ru.sadkov.tm.command.data;

import ru.sadkov.tm.command.AbstractCommand;

public final class DomainSaveXMLCommand extends AbstractCommand {

    @Override
    public String command() {
        return "save-XML";
    }

    @Override
    public String description() {
        return "Save domain in XML";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SAVING TO FILE]");
        if(serviceLocator.getDomainEndpoint().domainSaveXML(serviceLocator.getCurrentSession())) return;
        System.out.println("ACCESS FORBIDDEN");
    }

    @Override
    public boolean safe() {
        return true;
    }
}
