package ru.sadkov.tm.command.data;

import ru.sadkov.tm.command.AbstractCommand;

public final class DomainJacksonLoadXMLCommand extends AbstractCommand {

    @Override
    public String command() {
        return "load-jackson-XML";
    }

    @Override
    public String description() {
        return "Load domain from XML with jackson";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[READING FROM XML]");
        if(serviceLocator.getDomainEndpoint().domainLoadJacksonXML(serviceLocator.getCurrentSession())) return;
        System.out.println("ACCESS FORBIDDEN");
    }

    @Override
    public boolean safe() {
        return true;
    }
}
