package ru.sadkov.tm.command.task;

import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.endpoint.Task;
import ru.sadkov.tm.exception.WrongDataException;
import ru.sadkov.tm.util.ListShowUtil;

import java.util.List;

public final class TaskSortByDateEndCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task-sort-by-end";
    }

    @Override
    public String description() {
        return "show tasks sorted by end date";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASKS SORTED BY END DATE]");
        if (serviceLocator.getCurrentSession() == null) throw new WrongDataException();
        @Nullable final List<Task> tasks = serviceLocator.getTaskEndpoint()
                .getSortedTaskList(serviceLocator.getCurrentSession(), "taskDateEnd");
        ListShowUtil.showList(tasks);
    }

    @Override
    public boolean safe() {
        return false;
    }
}
