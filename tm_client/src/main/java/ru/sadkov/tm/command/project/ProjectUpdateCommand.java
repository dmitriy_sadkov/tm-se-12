package ru.sadkov.tm.command.project;


import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.endpoint.Exception_Exception;
import ru.sadkov.tm.endpoint.Project;
import ru.sadkov.tm.exception.WrongDataException;

public final class ProjectUpdateCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project-update";
    }

    @Override
    public String description() {
        return "Update project";
    }

    @Override
    public void execute() throws WrongDataException, Exception_Exception {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("[ENTER PROJECT NAME]");
        @Nullable final String projectName = serviceLocator.getScanner().nextLine();
        if (projectName == null || projectName.isEmpty() || serviceLocator.getCurrentSession() == null)
            throw new WrongDataException("[INCORRECT NAME]");
        @Nullable final Project project = serviceLocator.getProjectEndpoint()
                .findOneProjectByName(serviceLocator.getCurrentSession(), projectName);
        if (project == null) throw new WrongDataException("[ACCESS DENIED]");
        System.out.println("[ENTER NEW NAME]");
        @Nullable final String newName = serviceLocator.getScanner().nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        @Nullable final String description = serviceLocator.getScanner().nextLine();
        serviceLocator.getProjectEndpoint().updateProject(serviceLocator.getCurrentSession(), project, newName, description);
    }

    @Override
    public boolean safe() {
        return false;
    }
}
