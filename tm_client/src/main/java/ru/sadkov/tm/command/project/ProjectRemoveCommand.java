package ru.sadkov.tm.command.project;


import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.endpoint.Exception_Exception;
import ru.sadkov.tm.exception.WrongDataException;

public final class ProjectRemoveCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project-removeByName";
    }

    @Override
    public String description() {
        return "Remove selected project";
    }

    @Override
    public void execute() throws WrongDataException, Exception_Exception {
        System.out.println("[PROJECT REMOVE]");
        System.out.println("[ENTER NAME:]");
        @Nullable final String projectName = serviceLocator.getScanner().nextLine();
        if (projectName == null || projectName.isEmpty() || serviceLocator.getCurrentSession() == null)
            throw new WrongDataException("[INCORRECT NAME]");
        serviceLocator.getTaskEndpoint().removeTaskForProject(serviceLocator.getCurrentSession(),projectName);
        serviceLocator.getProjectEndpoint().removeProjectByName(serviceLocator.getCurrentSession(),projectName);
    }

    @Override
    public boolean safe() {
        return false;
    }
}
