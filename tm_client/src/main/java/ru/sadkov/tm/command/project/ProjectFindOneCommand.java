package ru.sadkov.tm.command.project;

import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.endpoint.Exception_Exception;
import ru.sadkov.tm.endpoint.Project;
import ru.sadkov.tm.exception.WrongDataException;

public final class ProjectFindOneCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project-show";
    }

    @Override
    public String description() {
        return "Show project";
    }

    @Override
    public void execute() throws WrongDataException, Exception_Exception {
        System.out.println("[SHOW PROJECT]");
        System.out.println("[ENTER PROJECT NAME]");
        @Nullable final String projectName = serviceLocator.getScanner().nextLine();
        @Nullable final Project project = serviceLocator.getProjectEndpoint()
                .findOneProjectByName(serviceLocator.getCurrentSession(),projectName);
        if (project == null) throw new WrongDataException("[INCORRECT NAME]");
        System.out.println(project);
    }

    @Override
    public boolean safe() {
        return false;
    }
}
