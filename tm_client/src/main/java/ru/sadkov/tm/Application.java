package ru.sadkov.tm;

import ru.sadkov.tm.launcher.Bootstrap;

/**
 * Hello world!
 */
public class Application {
    public static void main(String[] args) {
       // System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        new Bootstrap().init();
    }
}
