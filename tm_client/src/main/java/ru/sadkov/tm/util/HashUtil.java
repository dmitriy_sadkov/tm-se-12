package ru.sadkov.tm.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@UtilityClass
public final class HashUtil {
    @Nullable
    public static String hashMD5(@Nullable final String password) throws NoSuchAlgorithmException {
        if (password == null) return null;
        try {
            @NotNull final MessageDigest md =
                    MessageDigest.getInstance("MD5");
            @NotNull final byte[] array = md.digest(password.getBytes());
            @NotNull final StringBuilder sb = new StringBuilder();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100)
                        .substring(1, 3));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;

    }
}
